// Load plugins
import gulp from 'gulp';
const { series, parallel, src, dest, task } = gulp;
import pug from 'gulp-pug';
import merge from 'gulp-merge-json';
// https://github.com/sass/dart-sass/issues/2011
import * as dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import gulpConnect from 'gulp-connect';
import { deleteAsync } from 'del';

// ES6 modules (imports and exports) as they are not yet supported natively in Node
// so I'm using gulp.task instead of commonJs exports.aTaskName = aFunc
task(clean);
clean.description = 'Clean the build directory';
task('server', webserver);
task('server').description = 'Local dev web server';
task('build',
    series(
        series(pug_data, pug_src),
        parallel(
            bulma, bulmajs, minisearch, js,
            fontawesome, css
)));
task('build').description = 'Build the static website';
task('default', series('clean', 'build'));
task('default').description = 'clean + build';

function clean() {
  // You can use multiple globbing patterns as you would with `gulp.src`
  return deleteAsync(['build']);
};

// aggregate different JSON files into one database
function pug_data() {
  return src('data/**/*.json') // https://github.com/joshswan/gulp-merge-json/issues/32
      .pipe(merge({
          fileName: 'writeups.json',
          startObj: [],
          concatArrays: true,
          mergeArrays: true,
          jsonSpace: '  '
      }))
      .pipe(dest('build/'));
};

// compile pug templates into HTML and pass data in argument
function pug_src() {
  return src('pug/*.pug')
      .pipe(pug({
          pretty: true,
      }))
      .pipe(dest('build/'));
};

// build the customized bulma CSS
function bulma() {
  return src('sass/bulma.sass')
      .pipe(sass())
      .pipe(dest('build/css/vendor/bulma/'));
};

function bulmajs() {
  return src('node_modules/@vizuaalog/bulmajs/dist/navbar.js', { buffer: false })
      .pipe(dest('build/js/vendor/bulmajs/'));
};

function minisearch() {
  return src('node_modules/minisearch/dist/umd/index.js', { buffer: false })
      .pipe(dest('build/js/vendor/minisearch/'));
};

// copy personal (non-vendor) scripts
function js() {
  return src('js/**/*.js', { buffer: false })
      .pipe(dest('build/js/'));
};

// build personal (non-vendor) css
function css() {
  return src('sass/site.sass')
      .pipe(sass())
      .pipe(dest('build/css/'));
};

function fontawesome() {
  return src(['node_modules/@fortawesome/fontawesome-free/css/*',
  'node_modules/@fortawesome/fontawesome-free/webfonts/*'],
  {
    base: 'node_modules/@fortawesome/fontawesome-free/',
    // buffer: false, // https://github.com/gulpjs/gulp/issues/2768#issuecomment-2030071554
    encoding: false // https://github.com/gulpjs/gulp/issues/2766
  })
      .pipe(dest('build/css/vendor/fontawesome/'));
};

function webserver() {
  gulpConnect.server({
    root: 'build',
    port: 3000
  });
};
