let miniSearch = new MiniSearch({
  fields: ['ctf', 'challenge', 'keywords', 'author'],
  storeFields: ['ctf', 'challenge', 'author', 'lang', 'link', 'keywords'],
  //idField: 'link', // https://github.com/lucaong/minisearch/issues/59#issuecomment-841691275
  extractField: (document, fieldName) => {
    if (fieldName === 'id') {
      return `${document.ctf}-${document.challenge}-${document.author}-${document.lang}`
    } else {
      return document[fieldName]
    }
  },
  searchOptions: {
    boost: {challenge: 2, keywords: 1.2},
    prefix: true,
  }
});

fetch('https://writeup.raw.pm/writeups.json')
  .then(res => res.json())
  .then(function(data) {
    miniSearch.addAll(data);
  });

  let searchInput = function (val) {
    if (val.length > 2) {
      output = document.getElementById("output");
      output.innerHTML = "";
      res = miniSearch.search(val);
      if (res.length > 0) {
        for (elem of res) {
          div = document.createElement("div");
          par = document.createElement("p");
          control = div.cloneNode(true);
          control.classList = "control";
          tags = div.cloneNode(true);
          tags.classList = "tags has-addons";
          tag1 = div.cloneNode(true);
          tag1.classList = "tag is-dark";
          tag2 = div.cloneNode(true);
          tag2.classList = "tag";
          tag_keyword = document.createElement("span");
          tag_keyword.classList = "tag is-primary";
          card_footer_item = document.createElement("a");
          card_footer_item.classList = "card-footer-item";

          column = div.cloneNode(true);
          column.classList = "column is-full-mobile is-half-tablet is-one-third-widescreen is-one-quarter-desktop is-one-quarter-fullhd";
          card = div.cloneNode(true);
          card.classList = "card card-equal-height";
          card_header = document.createElement("header");
          card_header.classList = "card-header";
          card_header_title = par.cloneNode(true);
          card_header_title.classList = "card-header-title";
          card_header_title.innerText = `[${elem.ctf}] ${elem.challenge}`;
          card_header.appendChild(card_header_title);
          card.appendChild(card_header);
          card_content = div.cloneNode(true);
          card_content.classList = "card-content";
          content = div.cloneNode(true);
          content.classList = "content";
          /*description = par.cloneNode(true);
          description.innerHTML = elem.description;
          content.appendChild(description);*/
          field = div.cloneNode(true);
          field.classList = "field is-grouped is-grouped-multiline";
          if (elem.ctf) {
            control_ctf = control.cloneNode(true);
            tags_ctf = tags.cloneNode(true);
            ctf = tag1.cloneNode(true);
            ctf.innerText = 'CTF/platform';
            tags_ctf.appendChild(ctf);
            ctf_value = tag2.cloneNode(true);
            ctf_value.classList.add('is-danger');
            ctf_value.innerText = elem.ctf;
            tags_ctf.appendChild(ctf_value);
            control_ctf.appendChild(tags_ctf);
            field.appendChild(control_ctf);
          }
          if (elem.challenge) {
            control_challenge = control.cloneNode(true);
            tags_challenge = tags.cloneNode(true);
            challenge = tag1.cloneNode(true);
            challenge.innerText = 'Challenge/room/box';
            tags_challenge.appendChild(challenge);
            challenge_value = tag2.cloneNode(true);
            challenge_value.classList.add('is-warning');
            challenge_value.innerText = elem.challenge;
            tags_challenge.appendChild(challenge_value);
            control_challenge.appendChild(tags_challenge);
            field.appendChild(control_challenge);
          }
          if (elem.author) {
            control_author = control.cloneNode(true);
            tags_author = tags.cloneNode(true);
            author = tag1.cloneNode(true);
            author.innerText = 'Author';
            tags_author.appendChild(author);
            author_value = tag2.cloneNode(true);
            author_value.classList.add('is-info');
            author_value.innerText = elem.author;
            tags_author.appendChild(author_value);
            control_author.appendChild(tags_author);
            field.appendChild(control_author);
          }
          if (elem.lang) {
            control_lang = control.cloneNode(true);
            tags_lang = tags.cloneNode(true);
            lang = tag1.cloneNode(true);
            lang.innerText = 'Lang';
            tags_lang.appendChild(lang);
            lang_value = tag2.cloneNode(true);
            lang_value.classList.add('is-success');
            lang_value.innerText = elem.lang;
            tags_lang.appendChild(lang_value);
            control_lang.appendChild(tags_lang);
            field.appendChild(control_lang);
          }
          content.appendChild(field);
          if (elem.keywords) {
            keywords = par.cloneNode(true);
            keywords.innerText = 'Keywords:';
            content.appendChild(keywords);
            keyword_tags = div.cloneNode(true);
            keyword_tags.classList = 'tags';
            for (tag of elem.keywords.split(', ')) {
              tag_x = tag_keyword.cloneNode(true);
              tag_x.innerText = tag;
              keyword_tags.appendChild(tag_x);
            }
            content.appendChild(keyword_tags);
          }
          card_content.appendChild(content);
          card.appendChild(card_content);
          if (elem.link) {
            footer = document.createElement("footer");
            footer.classList = "card-footer";
              link = card_footer_item.cloneNode(true);
              link.setAttribute('href', elem.link);
              link.innerText = 'Link';
              footer.appendChild(link);
            card.appendChild(footer);
          }
          column.appendChild(card);
          output.appendChild(column);
        }
      }
    }
  };
